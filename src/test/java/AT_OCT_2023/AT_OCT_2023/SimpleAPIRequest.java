
package AT_OCT_2023.AT_OCT_2023;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class SimpleAPIRequest {

	public static void main(String args[]) {
		RestAssured.baseURI = "https://www.google.com";
		RequestSpecification requestSpecification = RestAssured.given();
		Response response = requestSpecification.get();
		System.out.println(response.asPrettyString());
		System.out.println(response.statusCode());
		System.out.println(response.time());

	}

}
