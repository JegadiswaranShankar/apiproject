package AT_OCT_2023.AT_OCT_2023;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import com.api.utils.TestUtility;

import io.restassured.RestAssured;
import io.restassured.config.EncoderConfig;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.response.Response;

public class CreateJobRequest {

	public static void main(String[] args) {

		baseURI = "http://139.59.91.96:9000/v1";
		Header headerOne = new Header("Content-Type", "application-json");
		Header headerTwo = new Header("Authorization",
				"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NCwiZmlyc3RfbmFtZSI6ImZkIiwibGFzdF9uYW1lIjoiZmQiLCJsb2dpbl9pZCI6ImlhbWZkIiwibW9iaWxlX251bWJlciI6Ijg4OTk3NzY2NTUiLCJlbWFpbF9pZCI6Im1hcmtAZ21haWwuY29tIiwicGFzc3dvcmQiOiI1ZjRkY2MzYjVhYTc2NWQ2MWQ4MzI3ZGViODgyY2Y5OSIsInJlc2V0X3Bhc3N3b3JkX2RhdGUiOm51bGwsImxvY2tfc3RhdHVzIjowLCJpc19hY3RpdmUiOjEsIm1zdF9yb2xlX2lkIjo1LCJtc3Rfc2VydmljZV9sb2NhdGlvbl9pZCI6MSwiY3JlYXRlZF9hdCI6IjIwMjEtMTEtMDNUMDg6MDY6MjMuMDAwWiIsIm1vZGlmaWVkX2F0IjoiMjAyMS0xMS0wM1QwODowNjoyMy4wMDBaIiwicm9sZV9uYW1lIjoiRnJvbnREZXNrIiwic2VydmljZV9sb2NhdGlvbiI6IlNlcnZpY2UgQ2VudGVyIEEiLCJpYXQiOjE2NzExNzE4ODR9.smUauqrubRlk1lcWSX5f5fD_UW16X-EhSTd7RQMp4OY");
		Headers headers = new Headers(headerOne, headerTwo);
		Response response = given().headers(headers).and()
				.body(TestUtility.toJson(TestUtility.getCreateJobRequestBody())).and()
				.config(RestAssured.config().encoderConfig(
						EncoderConfig.encoderConfig().encodeContentTypeAs("application-json", ContentType.JSON)))
				.post("/job/create");
		// .then().log().all().extract().response();

		System.out.println(response.asPrettyString());

	}

}
