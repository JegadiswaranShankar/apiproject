
package AT_OCT_2023.AT_OCT_2023;

import static com.api.utils.TestUtility.toJson;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import com.api.pojo.LoginPojo;

import io.restassured.http.Header;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class LoginAPIRequest3 {

	public static void main(String args[]) {
		baseURI = "http://139.59.91.96:9000/v1";
		LoginPojo loginPojo = new LoginPojo("iamfd", "password");
		String loginRequestBody = toJson(loginPojo);
		Response response = given().header(new Header("Content-Type", "application/json")).body(loginRequestBody)
				.post("/login");
		System.out.println(response.asPrettyString());
		System.out.println(response.statusCode());
		System.out.println(response.time());
		// How to retrieve data from Response using rest assured
		JsonPath jsonPath = new JsonPath(response.asPrettyString());
		String token = jsonPath.getString("data.token");
		System.out.println(token);
		String message = jsonPath.getString("message");
		System.out.println(message);

	}

}
