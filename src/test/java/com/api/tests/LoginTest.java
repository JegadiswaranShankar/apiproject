package com.api.tests;

import static com.api.utils.TestUtility.toJson;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.api.pojo.LoginPojo;

import io.restassured.http.Header;

public class LoginTest {

	private String loginRequestBody;

	@BeforeMethod(description = "Setting up the base uri and the login POJO")
	public void setup() {
		baseURI = "http://139.59.91.96:9000/v1";
		LoginPojo loginPojo = new LoginPojo("iamfd", "password");
		loginRequestBody = toJson(loginPojo);
	}

	@Test(description = "verify if login api works for front desk", groups = { "e2e", "smoke", "sanity" }, priority = 1)
	public void test_login_api() {

		String message = given().header(new Header("Content-Type", "application/json")).body(loginRequestBody).when()
				.log().all().post("/login").then().assertThat().statusCode(200).and().extract().body().jsonPath()
				.getString("message");

		Assert.assertEquals(message, "Success");

	}

}
