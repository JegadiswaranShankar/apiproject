package com.api.utils;

import com.api.pojo.Customer;
import com.api.pojo.Customer_Address;
import com.api.pojo.Customer_Product;
import com.api.pojo.Problems;
import com.api.pojo.createJobPOJO;
import com.github.javafaker.Faker;
import com.google.gson.Gson;

public class TestUtility {

	public static String toJson(Object Pojo) {

		Gson gson = new Gson();
		String data = gson.toJson(Pojo);
		// System.out.println(data);
		return data;

	}

	public static createJobPOJO getCreateJobRequestBody() {
		Faker faker = new Faker();
		String firstName = faker.name().firstName();

		Customer customer = new Customer(firstName, faker.name().lastName(), faker.numerify("##########"),
				faker.numerify("##########"), faker.internet().emailAddress(), "");
		Customer_Address address = new Customer_Address(faker.numerify("###"), faker.numerify("###"),
				faker.address().streetAddress(), faker.address().streetAddress(), faker.address().streetAddress(),
				faker.numerify("######"), "India", "Maharastra");

		String imei = faker.numerify("##############");
		Customer_Product productInfo = new Customer_Product("2022-11-30T18:30:00.000Z", imei, imei, imei,
				"2022-11-30T18:30:00.000Z", 3, 3);
		Problems[] phoneProblems = new Problems[1];
		phoneProblems[0] = new Problems(1, "phone not working");

		createJobPOJO createJobPOJO = new createJobPOJO(1, 2, 1, 2, customer, address, productInfo, phoneProblems);

		// System.out.println(createJobPOJO);
		return createJobPOJO;
	}

}
